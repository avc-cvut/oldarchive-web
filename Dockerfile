FROM rustlang/rust:nightly@sha256:37de28380745d7c769b901781fa409f0bbb293357f16d78ebf793a1330b4b39f as build
WORKDIR /usr/src

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN apt-get update \
 && apt-get install -y musl-tools \
 && rm -rf /var/lib/apt/lists/* \
 && rustup target add x86_64-unknown-linux-musl

# trick to cache dependecies
# see https://github.com/rust-lang/cargo/issues/2644
WORKDIR /usr/src
RUN USER=root cargo new oldarchive-gdrive-http-gateway
WORKDIR /usr/src/oldarchive-gdrive-http-gateway

COPY Cargo.toml Cargo.lock ./
RUN cargo install --target x86_64-unknown-linux-musl --locked --path .
RUN rm -rf src target/release/.fingerprint/oldarchive-gdrive-http-gateway*

COPY db-data ./db-data
COPY src ./src

RUN cargo install --target x86_64-unknown-linux-musl --locked --path .

# Test that everything is staticly linked. Copy to /dev/stderr for observability
RUN ldd "/usr/local/cargo/bin/old-archive" | tee /dev/stderr | grep -q "statically linked"

ENTRYPOINT ["/usr/local/cargo/bin/old-archive"]

FROM alpine:3.12 as alpine-ca-certificates

RUN apk add --no-cache ca-certificates

FROM scratch as release
WORKDIR /

COPY --from=alpine-ca-certificates /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /usr/local/cargo/bin/old-archive /old-archive
COPY static /static
COPY templates /templates

ENTRYPOINT ["/old-archive"]
