#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ -z "${DATABASE_URL:-}" ]; then
  echo "You need to provide env var DATABASE_URL in form 'postgres://user:pass@host:port/db_name'"
  exit 1;
fi

function dump {
  docker run \
	--rm \
	--init \
	postgres:9.6.11-alpine \
		psql \
		"$DATABASE_URL" \
		--tuples-only \
    -c "SELECT array_to_json(array_agg(row_to_json(t))) FROM $1 t $2;" > "$DIR/$1.json"
}

set -x

dump avc_file ""
dump avc_event "WHERE status = 6"
dump avc_category ""
dump encode_format ""

