use std::collections::HashMap;
use std::sync::Arc;

use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

use parser::*;

mod parser;

#[derive(Serialize)] //For Templates
pub struct AvcCategory {
    pub id: i32,
    pub name: String,
    pub slug: String,
    pub parent_id: Option<i32>,
    pub description: String,
    pub path: String,
    _private: (),
}

#[allow(clippy::manual_non_exhaustive)]
pub struct Categories {
    pub by_id: HashMap<i32, Arc<AvcCategory>>,
    pub by_path: HashMap<String, Arc<AvcCategory>>,
    pub by_parent_id: HashMap<Option<i32>, Vec<Arc<AvcCategory>>>,
    _private: (),
}

impl Categories {
    pub fn new(data: Vec<AvcCategory>) -> Categories {
        let data: Vec<Arc<AvcCategory>> = data.into_iter().map(Arc::new).collect();

        let by_id: HashMap<i32, Arc<AvcCategory>> =
            data.iter().map(|i| (i.id, i.clone())).collect();
        let by_path: HashMap<String, Arc<AvcCategory>> =
            data.iter().map(|i| (i.path.clone(), i.clone())).collect();

        let mut by_parent_id: HashMap<Option<i32>, Vec<Arc<AvcCategory>>> = HashMap::new();
        for category in data {
            match by_parent_id.get_mut(&category.parent_id) {
                None => {
                    by_parent_id.insert(category.parent_id, vec![category.clone()]);
                }
                Some(vec) => {
                    vec.push(category.clone());
                }
            };
        }

        Categories {
            by_id,
            by_path,
            by_parent_id,
            _private: (),
        }
    }
}

#[allow(clippy::manual_non_exhaustive)]
#[derive(Serialize)] //For Templates
pub struct AvcFile {
    pub id: i32,
    pub event_id: i32,
    pub format_id: i32,
    pub path: String,
    pub note: String,
    pub added: DateTime<Utc>,
    pub size: Option<i32>,
    _private: (),
}

#[derive(Deserialize, Serialize)]
pub struct EncodeFormat {
    pub id: i32,
    pub name: String,
}

#[allow(clippy::manual_non_exhaustive)]
pub struct Files {
    pub by_id: HashMap<i32, Arc<AvcFile>>,
    pub by_event_id: HashMap<i32, Vec<Arc<AvcFile>>>,
    pub encode_format_by_id: HashMap<i32, EncodeFormat>,
    _private: (),
}

impl Files {
    pub fn new(data: Vec<AvcFile>, encode_formats: Vec<EncodeFormat>) -> Files {
        let data: Vec<Arc<AvcFile>> = data.into_iter().map(Arc::new).collect();

        let by_id: HashMap<i32, Arc<AvcFile>> = data.iter().map(|i| (i.id, i.clone())).collect();

        let mut by_event_id: HashMap<i32, Vec<Arc<AvcFile>>> = HashMap::new();
        for file in data {
            match by_event_id.get_mut(&file.event_id) {
                None => {
                    by_event_id.insert(file.event_id, vec![file.clone()]);
                }
                Some(vec) => {
                    vec.push(file.clone());
                }
            }
        }

        let encode_format_by_id: HashMap<i32, EncodeFormat> =
            encode_formats.into_iter().map(|i| (i.id, i)).collect();

        Files {
            by_id,
            by_event_id,
            encode_format_by_id,
            _private: (),
        }
    }
}

#[allow(clippy::manual_non_exhaustive)]
#[derive(Serialize)] //For Templates
pub struct AvcEvent {
    pub id: i32,
    pub name: String,
    pub slug: String,
    pub start: DateTime<Utc>,
    pub place: String,
    pub description: String,
    pub category_id: Option<i32>,
    _private: (),
}

#[allow(clippy::manual_non_exhaustive)]
pub struct Events {
    pub by_id: HashMap<i32, Arc<AvcEvent>>,
    pub by_slug: HashMap<String, Arc<AvcEvent>>,
    pub by_category_id: HashMap<Option<i32>, Vec<Arc<AvcEvent>>>,
    _private: (),
}

impl Events {
    pub fn new(data: Vec<AvcEvent>) -> Events {
        let data: Vec<Arc<AvcEvent>> = data.into_iter().map(Arc::new).collect();

        let by_id: HashMap<i32, Arc<AvcEvent>> = data.iter().map(|i| (i.id, i.clone())).collect();
        let by_slug: HashMap<String, Arc<AvcEvent>> =
            data.iter().map(|i| (i.slug.clone(), i.clone())).collect();

        let mut by_category_id: HashMap<Option<i32>, Vec<Arc<AvcEvent>>> = HashMap::new();
        for event in data {
            match by_category_id.get_mut(&event.category_id) {
                None => {
                    by_category_id.insert(event.category_id, vec![event.clone()]);
                }
                Some(vec) => {
                    vec.push(event.clone());
                }
            }
        }

        Events {
            by_id,
            by_slug,
            by_category_id,
            _private: (),
        }
    }
}

#[allow(clippy::manual_non_exhaustive)]
pub struct AvcData {
    pub categories: Categories,
    pub files: Files,
    pub events: Events,
    _private: (),
}

#[allow(clippy::manual_non_exhaustive)]
impl AvcData {
    pub fn new(
        categories: Vec<AvcCategory>,
        files: Vec<AvcFile>,
        events: Vec<AvcEvent>,
        encode_formats: Vec<EncodeFormat>,
    ) -> AvcData {
        AvcData {
            categories: Categories::new(categories),
            files: Files::new(files, encode_formats),
            events: Events::new(events),
            _private: (),
        }
    }
}

pub fn provide_data() -> AvcData {
    AvcData::new(
        parse_data_file::<AvcCategoryDb, AvcCategory>(include_str!(concat!(
            env!("CARGO_MANIFEST_DIR"),
            "/db-data/avc_category.json"
        ))),
        parse_data_file::<AvcFileDb, AvcFile>(include_str!(concat!(
            env!("CARGO_MANIFEST_DIR"),
            "/db-data/avc_file.json"
        ))),
        parse_data_file::<AvcEventDb, AvcEvent>(include_str!(concat!(
            env!("CARGO_MANIFEST_DIR"),
            "/db-data/avc_event.json"
        ))),
        parse_data_file::<EncodeFormat, EncodeFormat>(include_str!(concat!(
            env!("CARGO_MANIFEST_DIR"),
            "/db-data/encode_format.json"
        ))),
    )
}
