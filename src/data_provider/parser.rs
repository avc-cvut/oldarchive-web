use std::convert::TryFrom;

use chrono::{DateTime, Utc};
use serde::{de::DeserializeOwned, Deserialize};

use crate::data_provider::{AvcCategory, AvcEvent, AvcFile};

#[derive(Deserialize)]
pub(super) struct AvcCategoryDb {
    pub id: i32,
    pub name: String,
    pub slug: String,
    pub parent_id: Option<i32>,
    pub description: String,
    pub path: String,
}

impl TryFrom<AvcCategoryDb> for AvcCategory {
    type Error = ();

    fn try_from(value: AvcCategoryDb) -> Result<Self, Self::Error> {
        Ok(AvcCategory {
            id: value.id,
            name: value.name,
            slug: value.slug,
            parent_id: value.parent_id,
            description: value.description,
            path: value.path,
            _private: (),
        })
    }
}

#[derive(Deserialize)]
pub(super) struct AvcEventDb {
    pub id: i32,
    pub name: String,
    pub slug: String,
    pub start: DateTime<Utc>,
    pub place: Option<String>,
    pub description: String,
    pub status: i32,
    pub category_id: Option<i32>,
}

impl TryFrom<AvcEventDb> for AvcEvent {
    type Error = ();

    fn try_from(value: AvcEventDb) -> Result<Self, Self::Error> {
        if value.status != 6 {
            return Err(());
        }

        Ok(AvcEvent {
            id: value.id,
            name: value.name,
            slug: value.slug,
            start: value.start,
            place: value.place.nullable_to_empty_string(),
            description: value.description,
            category_id: value.category_id,
            _private: (),
        })
    }
}

#[derive(Deserialize)]
pub(super) struct AvcFileDb {
    pub id: i32,
    pub event_id: i32,
    pub format_id: Option<i32>,
    pub path: String,
    pub note: Option<String>,
    pub added: DateTime<Utc>,
    pub size: Option<i32>,
    pub public: bool,
}

impl TryFrom<AvcFileDb> for AvcFile {
    type Error = ();

    fn try_from(value: AvcFileDb) -> Result<Self, Self::Error> {
        if !value.public {
            return Err(());
        }

        let path = if value.path.contains("://") {
            value.path
        } else {
            format!("http://media.avc-cvut.cz/{}", value.path)
        };

        Ok(AvcFile {
            id: value.id,
            event_id: value.event_id,
            format_id: value.format_id.unwrap_or(5), //5 = n/a
            path,
            note: value.note.nullable_to_empty_string(),
            added: value.added,
            size: value.size,
            _private: (),
        })
    }
}

trait NullableEmptyString {
    fn nullable_to_empty_string(self) -> String;
}

impl NullableEmptyString for Option<String> {
    fn nullable_to_empty_string(self) -> String {
        match self {
            Some(s) => s,
            None => "".to_string(),
        }
    }
}

pub(super) fn parse_data_file<ParseType: DeserializeOwned, OutputType: TryFrom<ParseType>>(
    data: &str,
) -> Vec<OutputType> {
    let data: Vec<ParseType> = serde_json::from_str(data).unwrap();

    data.into_iter()
        .map(OutputType::try_from)
        .filter(|o| o.is_ok())
        .map(|o| o.unwrap_or_else(|_| panic!("This should never happen")))
        .collect()
}
