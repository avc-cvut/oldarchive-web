pub mod archive;
pub mod event;
pub mod file;

#[get("/")]
pub fn root_route() -> rocket::response::Redirect {
    rocket::response::Redirect::temporary("/archiv/")
}

#[derive(serde::Serialize)]
struct WithUrl<T> {
    pub data: T,
    pub url: String,
}
