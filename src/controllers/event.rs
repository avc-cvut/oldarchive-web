use std::sync::Arc;

use rocket::response::status::NotFound;
use rocket_contrib::templates::Template;
use serde::Serialize;

use crate::controllers::WithUrl;
use crate::data_provider::{AvcCategory, AvcEvent, AvcFile};

#[derive(Serialize)]
struct TemplateData {
    event: Arc<AvcEvent>,
    ancestors: Vec<WithUrl<Arc<AvcCategory>>>,
    files: Vec<AvcFileWithType>,
}

#[derive(Serialize)]
struct AvcFileWithType {
    pub file: WithUrl<Arc<AvcFile>>,
    pub format: String,
}

#[get("/akce/<slug>")]
pub fn event_route(slug: String) -> Result<Template, NotFound<&'static str>> {
    let event = crate::AVC_DATA
        .events
        .by_slug
        .get(&slug)
        .ok_or(NotFound("404 Not Found"))?
        .clone();

    let mut ancestors = Vec::new();
    if let Some(category_id) = event.category_id {
        let category = crate::AVC_DATA.categories.by_id.get(&category_id);
        if let Some(category) = category {
            ancestors = super::archive::get_ancestors(category.clone());
        }
    }

    let files = child_files(&event.id);

    Ok(Template::render(
        "event",
        TemplateData {
            event,
            ancestors,
            files,
        },
    ))
}

fn child_files(event_id: &i32) -> Vec<AvcFileWithType> {
    crate::AVC_DATA
        .files
        .by_event_id
        .get(event_id)
        .unwrap_or(&Vec::new())
        .iter()
        .map(WithUrl::from)
        .map(|file| {
            let format = crate::AVC_DATA
                .files
                .encode_format_by_id
                .get(&file.data.format_id)
                .map(|i| i.name.clone())
                .unwrap_or_else(|| "N/A".to_string());

            AvcFileWithType { file, format }
        })
        .collect()
}

impl From<&Arc<AvcEvent>> for WithUrl<Arc<AvcEvent>> {
    fn from(event: &Arc<AvcEvent>) -> Self {
        WithUrl {
            data: event.clone(),
            url: uri!(event_route: slug = &event.slug).to_string(),
        }
    }
}

#[cfg(test)]
mod tests {
    use rocket::local::Client;

    use crate::create_server;

    #[test]
    fn event_path_test_only_code() {
        let client = Client::new(create_server()).expect("valid rocket instance");

        let req = client.get("/akce/ohlednuti-za-show-2010/");
        let response = req.dispatch();

        println!("{:?}", response);
        assert_eq!(response.status().code, 200);
    }

    #[test]
    fn event_path_with_trailing_slash_test_only_code() {
        let client = Client::new(create_server()).expect("valid rocket instance");

        let req = client.get("/akce/ohlednuti-za-show-2010/");
        let response = req.dispatch();

        println!("{:?}", response);
        assert_eq!(response.status().code, 200);
    }

    #[test]
    fn event_path_not_found_test_only_code() {
        let client = Client::new(create_server()).expect("valid rocket instance");

        let req = client.get("/akce/asdfasdf/");
        let response = req.dispatch();

        println!("{:?}", response);
        assert_eq!(response.status().code, 404);
    }
}
