use crate::controllers::WithUrl;
use crate::data_provider::AvcFile;
use rocket::response::{status::NotFound, Redirect};
use std::sync::Arc;

#[get("/stahnout/<id>")]
pub fn file_route(id: i32) -> Result<Redirect, NotFound<&'static str>> {
    let file: &Arc<AvcFile> = crate::AVC_DATA
        .files
        .by_id
        .get(&id)
        .ok_or(NotFound("404 Not Found"))?;

    Ok(Redirect::temporary(file.path.clone()))
}

impl From<&Arc<AvcFile>> for WithUrl<Arc<AvcFile>> {
    fn from(file: &Arc<AvcFile>) -> Self {
        WithUrl {
            data: file.clone(),
            url: uri!(file_route: id = &file.id).to_string(),
        }
    }
}

#[cfg(test)]
mod tests {
    use rocket::local::Client;

    use crate::create_server;

    #[test]
    fn test_file() {
        let client = Client::new(create_server()).expect("valid rocket instance");

        let req = client.get("/stahnout/3114");
        let response = req.dispatch();

        assert_eq!(response.status().code, 307);
        assert_eq!(response.headers().get_one("Location").unwrap(), "https://media.avc-cvut.cz/ARCHIV/AVC/Jednorazove_akce/2005_03_29_Diskuze_k_evaluacni_zprave.avi");
    }

    #[test]
    fn test_file_not_found() {
        let client = Client::new(create_server()).expect("valid rocket instance");

        let req = client.get("/stahnout/31144532");
        let response = req.dispatch();

        assert_eq!(response.status().code, 404);
        println!("{:?}", response);
    }
}
