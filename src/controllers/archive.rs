use std::path::PathBuf;
use std::sync::Arc;

use rocket::response::status::NotFound;
use rocket_contrib::templates::Template;
use serde::Serialize;

use crate::controllers::WithUrl;
use crate::data_provider::{AvcCategory, AvcEvent};

#[derive(Serialize)]
struct TemplateData {
    current_category: Option<Arc<AvcCategory>>,
    ancestors: Vec<WithUrl<Arc<AvcCategory>>>,
    child_categories: Vec<WithUrl<Arc<AvcCategory>>>,
    events: Vec<WithUrl<Arc<AvcEvent>>>,
}

#[get("/archiv")]
pub fn archive_tree_root_route() -> Template {
    let child_categories = child_categories(&None);
    let events = child_events(&None);

    Template::render(
        "archive_tree",
        TemplateData {
            current_category: None,
            ancestors: Vec::new(),
            child_categories,
            events,
        },
    )
}

#[get("/archiv/<path..>")]
pub fn archive_tree_route(path: PathBuf) -> Result<Template, NotFound<&'static str>> {
    let path = path.as_path().to_str().unwrap().to_string();

    let current_category = crate::AVC_DATA
        .categories
        .by_path
        .get(&path)
        .ok_or(NotFound("404 Not Found"))?
        .clone();

    let child_categories = child_categories(&Some(current_category.id));
    let events = child_events(&Some(current_category.id));

    Ok(Template::render(
        "archive_tree",
        TemplateData {
            current_category: Some(current_category.clone()),
            ancestors: get_ancestors(current_category),
            child_categories,
            events,
        },
    ))
}

fn child_categories(category_id: &Option<i32>) -> Vec<WithUrl<Arc<AvcCategory>>> {
    crate::AVC_DATA
        .categories
        .by_parent_id
        .get(category_id)
        .unwrap_or(&Vec::new())
        .iter()
        .map(WithUrl::from)
        .collect()
}

fn child_events(category_id: &Option<i32>) -> Vec<WithUrl<Arc<AvcEvent>>> {
    crate::AVC_DATA
        .events
        .by_category_id
        .get(category_id)
        .unwrap_or(&Vec::new())
        .iter()
        .map(WithUrl::from)
        .collect()
}

pub(super) fn get_ancestors(category: Arc<AvcCategory>) -> Vec<WithUrl<Arc<AvcCategory>>> {
    fn get_ancestors_inner(
        category: Arc<AvcCategory>,
        acc: Vec<WithUrl<Arc<AvcCategory>>>,
    ) -> Vec<WithUrl<Arc<AvcCategory>>> {
        let mut acc = acc;
        acc.push(WithUrl::from(category.clone()));

        match category.parent_id {
            None => acc,
            Some(parent_id) => get_ancestors_inner(
                crate::AVC_DATA
                    .categories
                    .by_id
                    .get(&parent_id)
                    .unwrap()
                    .clone(),
                acc,
            ),
        }
    }

    let mut result = get_ancestors_inner(category, Vec::new());
    result.reverse();

    result
}

impl From<Arc<AvcCategory>> for WithUrl<Arc<AvcCategory>> {
    fn from(category: Arc<AvcCategory>) -> Self {
        WithUrl {
            data: category.clone(),
            url: uri!(archive_tree_route: path = category.path.split('/').collect::<PathBuf>())
                .to_string(),
        }
    }
}

impl From<&Arc<AvcCategory>> for WithUrl<Arc<AvcCategory>> {
    fn from(category: &Arc<AvcCategory>) -> Self {
        WithUrl {
            data: category.clone(),
            url: uri!(archive_tree_route: path = category.path.split('/').collect::<PathBuf>())
                .to_string(),
        }
    }
}

#[cfg(test)]
mod tests {
    use rocket::local::Client;

    use crate::create_server;

    #[test]
    fn archive_homepage_without_trailing_slash_test_only_code() {
        let client = Client::new(create_server()).expect("valid rocket instance");

        let req = client.get("/archiv");
        let response = req.dispatch();

        println!("{:?}", response);
        assert_eq!(response.status().code, 200);
    }

    #[test]
    fn archive_homepage_with_trailing_slash_test_only_code() {
        let client = Client::new(create_server()).expect("valid rocket instance");

        let req = client.get("/archiv");
        let response = req.dispatch();

        println!("{:?}", response);
        assert_eq!(response.status().code, 200);
    }

    #[test]
    fn archive_path_test_only_code() {
        let client = Client::new(create_server()).expect("valid rocket instance");

        let req = client.get("/archiv/studentske-deni/show");
        let response = req.dispatch();

        println!("{:?}", response);
        assert_eq!(response.status().code, 200);
    }

    #[test]
    fn archive_path_with_trailing_slash_test_only_code() {
        let client = Client::new(create_server()).expect("valid rocket instance");

        let req = client.get("/archiv/studentske-deni/show/");
        let response = req.dispatch();

        println!("{:?}", response);
        assert_eq!(response.status().code, 200);
    }

    #[test]
    fn archive_path_not_found_test_only_code() {
        let client = Client::new(create_server()).expect("valid rocket instance");

        let req = client.get("/archiv/studentske-asdfdeni/show/");
        let response = req.dispatch();

        println!("{:?}", response);
        assert_eq!(response.status().code, 404);
    }
}
