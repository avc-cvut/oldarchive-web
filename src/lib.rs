#![feature(proc_macro_hygiene, decl_macro)]
#![allow(clippy::manual_non_exhaustive)]

#[macro_use]
extern crate rocket;

use lazy_static::lazy_static;
use rocket::Rocket;
use rocket_contrib::{serve::StaticFiles, templates::Template};

mod controllers;
mod data_provider;

lazy_static! {
    static ref AVC_DATA: data_provider::AvcData = data_provider::provide_data();
}

pub fn create_server() -> Rocket {
    rocket::ignite()
        .mount(
            "/",
            routes![
                controllers::root_route,
                controllers::archive::archive_tree_root_route,
                controllers::archive::archive_tree_route,
                controllers::event::event_route,
                controllers::file::file_route
            ],
        )
        .mount("/static", StaticFiles::from("/static"))
        .attach(Template::fairing())
}

#[cfg(test)]
mod tests {
    use rocket::local::Client;

    use super::*;

    #[test]
    fn root() {
        let client = Client::new(create_server()).expect("valid rocket instance");

        let req = client.get("/");
        let response = req.dispatch();

        assert_eq!(response.status().code, 307);
        assert_eq!(response.headers().get_one("Location").unwrap(), "/archiv/");
    }
}
